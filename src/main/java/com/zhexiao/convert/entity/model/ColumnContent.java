package com.zhexiao.convert.entity.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


/**
 * @author zhe.xiao
 * @date 2021-02-05 17:15
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ColumnContent {
    //第几列，从0开始
    private Integer columnNum = 0;

    private CellContent[] columnData = null;
}
