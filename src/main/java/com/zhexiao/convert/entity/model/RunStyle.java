package com.zhexiao.convert.entity.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author zhe.xiao
 * @date 2021-01-20 15:47
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class RunStyle implements Style {
    private Integer fontSize;
    private Boolean bold;
    private String color;
    private String fontFamily;
}

