package com.zhexiao.convert.entity.postman;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhe.xiao
 * @date 2020/09/16
 * @description
 */
@Data
@Accessors(chain = true)
public class Parameter {
    private String key;
    private String value;
}
