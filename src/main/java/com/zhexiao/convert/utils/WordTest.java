package com.zhexiao.convert.utils;


import com.zhexiao.convert.entity.model.CellContent;
import com.zhexiao.convert.entity.model.ColumnContent;
import com.zhexiao.convert.entity.model.ParagraphStyle;
import com.zhexiao.convert.entity.model.RunStyle;
import org.apache.poi.xwpf.usermodel.*;


public class WordTest {
    public static void main(String[] args) {
        Word word = new Word();
        Word.ParagraphBuilder.builder(word).setText("第二周调查问卷")
                .setParagraphStyle(new ParagraphStyle().setAlignment(ParagraphAlignment.CENTER))
                .setRunStyle(new RunStyle().setBold(true).setFontSize(20))
                .build();
        Word.ParagraphBuilder.builder(word).setText("1. 疼痛评分：6级").build();
        Word.ParagraphBuilder.builder(word).setText("2.与对侧相比肿胀程度：中度肿胀").build();
        Word.ParagraphBuilder.builder(word).setText("3.异常症状缓解情况：关节不稳").build();

        word.addBreak();

        //表格
        String[] columnData = {"姓名", "接口URL", "接口功能", "请求方式", "调用系统", "数据格式", "参数", "返回值", "返回示例", "调用举例", "异常场景"};
        RunStyle runStyle = new RunStyle().setFontSize(9).setColor("ff0000");

        CellContent[] cellContents = new CellContent[columnData.length];
        for (int i = 0; i < columnData.length; i++) {
            CellContent cellContent = new CellContent().setRunStyle(runStyle).setContent(columnData[i]);
            cellContents[i] = cellContent;
        }

        ColumnContent columnContent = new ColumnContent()
                .setColumnNum(0)
                .setColumnData(cellContents);

        Word.TableBuilder.builder(word)
                .setColumnContent(columnContent)
                .build();


        word.save("./test124.docx");
    }
}